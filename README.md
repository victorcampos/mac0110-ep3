# MAC0110-EP3

EP3 Coursework for MAC0110

This coursework will is based off of John Conway's Game of Life, 
by using a matrix composed of rabbits, wolves, grass blocks and carrots,
and simulating its behaviour over several given instants according to the 
amounts of each initial element and their probabilities of reproduction
and death given by a preset energy value for each element.

In this version, carrots serve as food for rabbits, which in part
serve as food for wolves. Grass blocks may also grow carrots.
simula(n) function will run the simulation for a given n number of
instances, whereas gera_graficos(n) function will plot the numbers of
wolves x rabbits and the numbers of food x total energy for a n value 
of instances. Initial values for both functions are set for 45.
